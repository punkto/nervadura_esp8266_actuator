#ifndef USE_THIS_CONF
#define USE_THIS_CONF

// Update these with values suitable for your network.
const char* ssid = "your network name";
const char* password = "your network pass";
const char* mqtt_server = "your MQTT broker URI";

#endif //USE_THIS_CONF
