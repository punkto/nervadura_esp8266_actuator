/*
 MQTT Light sensor for the Nervadura project
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Use here your configuration file or fill configuration.h with propper values
#include "configuration_alternative.h"
#include "configuration.h"

const int actuator_pin = D5;

#define MILLIS_TO_SEND_DEVICE_MSG 10000
#define MAX_MSG_SIZE 128
#define DEVICE_ID_SUFIX "act-"

const char* topic_name_devices = "nervadura/devices";
const char* topic_name_actuator_triggered = "nervadura/actuator_triggered";
const char* topic_name_test_led = "nervadura/test_led";

const int mqtt_sub_qos = 1;
String clientId;
String str_all; // When initialized at setup() will contain "ALL "
String str_ends_with_0;
String str_ends_with_1;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Returns the string sent to the devices topic. Currently, this is the device's client ID
const char * get_device_msg(){
  return clientId.c_str();
}

bool this_message_addresses_me(String msg) {
  if (msg.startsWith(str_all)) {
    return true;
  }
  if (msg.startsWith(clientId)) {
    return true;
  }
  return false;
}

bool this_message_turns_me_on(String msg) {
  if (msg.endsWith(str_ends_with_1)) {
    return true;
  }
  return false;
}

bool this_message_turns_me_off(String msg) {
  if (msg.endsWith(str_ends_with_0)) {
    return true;
  }
  return false;
}

char payload_copy[MQTT_MAX_PACKET_SIZE + 1];

// Function called when a message arrives. The only topic the sensor is suscribed in is the test_led.
void callback(char* topic, byte* payload, unsigned int length) {
  for (uint i=0; i < length; i++){
    payload_copy[i] = (char) payload[i];
  }
  payload_copy[length] = 0;
  String msg = String(payload_copy);
  String topic_as_string = String(topic);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println(msg.c_str());
  Serial.println(msg.length());

  int pin = BUILTIN_LED;
  if (topic_as_string.equals(String(topic_name_actuator_triggered))){
    pin = actuator_pin;
    Serial.println("Using D5 as output");
  }
  
  if (this_message_addresses_me(msg)) {
    Serial.println("This message is for me!");
    if (this_message_turns_me_on(msg)) {
      Serial.println("Turning on");
      digitalWrite(pin, HIGH);
    } else if (this_message_turns_me_off(msg)) {
      Serial.println("Turning off");
      digitalWrite(pin, LOW);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish(topic_name_devices, get_device_msg());
      client.subscribe(topic_name_test_led, mqtt_sub_qos);
      client.subscribe(topic_name_actuator_triggered, mqtt_sub_qos);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(actuator_pin, OUTPUT);
  digitalWrite(actuator_pin, LOW);
  uint32_t chipid;
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  chipid=ESP.getChipId();
  clientId = DEVICE_ID_SUFIX;
  clientId += String(chipid, HEX); // To access the client ID as a str use clientId.c_str()
  str_all = String("ALL ");
  str_ends_with_0 = String(" 0");
  str_ends_with_1 = String(" 1");
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

long last_device_msg_sent_millis = 0;

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  long now = millis();
  
  if (now - last_device_msg_sent_millis > MILLIS_TO_SEND_DEVICE_MSG) {
    last_device_msg_sent_millis = now;
    Serial.print("Publish message: ");
    Serial.println(get_device_msg());
    client.publish(topic_name_devices, get_device_msg());
  }
}
